import 'package:client_test_project/pages/home_page.dart';
import 'package:client_test_project/utils/app_string.dart';
import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';

void main() {
  runApp(const Application());
}

class Application extends StatelessWidget {
  const Application({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: AppString.applicationName,
      builder: (context, child) => ResponsiveWrapper.builder(HomePage(),
          // BouncingScrollWrapper.builder(context, child!),

          maxWidth: double.infinity,
          minWidth: 450,
          defaultScale: true,
          breakpoints: [
            ResponsiveBreakpoint.resize(450, name: MOBILE),
            ResponsiveBreakpoint.autoScale(800, name: MOBILE),
            ResponsiveBreakpoint.autoScale(800, name: TABLET),
            ResponsiveBreakpoint.autoScale(1000, name: TABLET),
            ResponsiveBreakpoint.resize(1200, name: DESKTOP),
            ResponsiveBreakpoint.autoScale(2460, name: "4K"),
          ],
          background: Container(color: Color(0xFFF5F5F5))),
      home: const HomePage(),
      // home: ScreenTypeLayout.builder(
      //   mobile: (BuildContext context) => const SplashPage(),
      //   desktop: (BuildContext context) => const HomePage(),
      //   tablet: (BuildContext context) => const SplashPage(),
      // ),
    );
  }
}
