import 'package:client_test_project/pages/sub_pages/first_tab.dart';
import 'package:client_test_project/pages/sub_pages/second_tab.dart';
import 'package:client_test_project/pages/sub_pages/third_tab.dart';
import 'package:client_test_project/utils/app_color.dart';
import 'package:client_test_project/utils/app_string.dart';
import 'package:client_test_project/utils/size_config.dart';
import 'package:client_test_project/widgets/banner_widget.dart';
import 'package:client_test_project/widgets/custom_button.dart';
import 'package:client_test_project/widgets/hover_text.dart';
import 'package:client_test_project/widgets/tab_widget.dart';
import 'package:contentsize_tabbarview/contentsize_tabbarview.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_builder/responsive_builder.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  BorderRadius? _indicatorBorderRadius;

  final ScrollController _scrollController = ScrollController();
  double _scrollPosition = 0;

  _scrollListener() {
    setState(() {
      _scrollPosition = _scrollController.position.pixels;
    });
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_scrollListener);

    _tabController =
        TabController(length: 3, vsync: this); // Update the length as needed
    _tabController.addListener(_handleTabSelection);
    _updateIndicatorBorderRadius(_tabController.index);
  }

  void _handleTabSelection() {
    setState(() {
      _updateIndicatorBorderRadius(_tabController.index);
    });
  }

  void _updateIndicatorBorderRadius(int index) {
    if (index == 0) {
      _indicatorBorderRadius = const BorderRadius.only(
        topLeft: Radius.circular(12),
        bottomLeft: Radius.circular(12),
      );
    } else if (index == _tabController.length - 1) {
      _indicatorBorderRadius = const BorderRadius.only(
        topRight: Radius.circular(12),
        bottomRight: Radius.circular(12),
      );
    } else {
      _indicatorBorderRadius = BorderRadius.circular(0);
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    return ResponsiveBuilder(builder: (context, sizingInformation) {
      return SafeArea(
        child: Scaffold(
          backgroundColor: AppColor.white,
          appBar: PreferredSize(
            preferredSize: const Size.fromHeight(67.0),
            child: Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 5,
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.topRight,
                      colors: <Color>[
                        AppColor.cyanDark,
                        AppColor.blueGradient,
                      ],
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: const BorderRadius.only(
                      bottomRight: Radius.circular(12),
                      bottomLeft: Radius.circular(12),
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        spreadRadius: 1,
                        blurRadius: 5,
                        offset: const Offset(0, 1),
                      ),
                    ],
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      (sizingInformation.isMobile)
                          ? const SizedBox(
                              width: 0,
                            )
                          : _scrollPosition >= 400
                              ? Align(
                                  alignment: Alignment.centerRight,
                                  child: registerWidget())
                              : const SizedBox(),
                      Padding(
                        padding: const EdgeInsets.only(right: 17.0, left: 20.0),
                        child: SizedBox(
                          height: 62,
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: HoverText(
                              text: AppString.login,
                              normalStyle: GoogleFonts.lato(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                color: AppColor.cyanDark,
                              ),
                              hoverStyle: GoogleFonts.lato(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                color: AppColor.cyanDark,
                              ),
                              hoverColor: AppColor
                                  .cyanList200, // Change this color to your desired hover color
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          bottomNavigationBar: (sizingInformation.isMobile)
              ? _bottomNavigationWidget()
              : const SizedBox(),
          body: ResponsiveBuilder(
            builder: (context, sizingInformation) {
              return ListView(
                scrollDirection: Axis.vertical,
                controller: _scrollController,
                shrinkWrap: true,
                children: [
                  const BannerWidget(),
                  const SizedBox(height: 40),
                  SizedBox(height: 40, child: _buildTabBar()),
                  const SizedBox(height: 20),
                  _buildTabBarView(),
                ],
              );
            },
          ),
        ),
      );
    });
  }

  Widget registerWidget() {
    return Row(
      children: [
        Text(
          'Jetzt Klicken',
          style: GoogleFonts.lato(
            textStyle: const TextStyle(
                color: AppColor.black, fontWeight: FontWeight.w600),
            fontSize: 19,
          ),
        ),
        const SizedBox(
          width: 20,
        ),
        Container(
            width: 200,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.0,
                color: AppColor.grey200,
              ),
              borderRadius: const BorderRadius.all(Radius.circular(
                      12.0) //                 <--- border radius here
                  ),
            ),
            height: 40,
            child: Center(
              child: Text(
                'Kostenlos Registrieren',
                style: GoogleFonts.lato(
                  textStyle: const TextStyle(
                      color: AppColor.cyanDark, fontWeight: FontWeight.w400),
                  fontSize: 14,
                ),
              ),
            )),
      ],
    );
  }

  Widget _buildTabBar() {
    return TabBar(
      tabAlignment: TabAlignment.center,
      controller: _tabController,
      overlayColor: WidgetStateProperty.all(Colors.transparent),
      tabs: [
        TabWidget(
          text: AppString.arbeitnehmer,
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(12),
            bottomLeft: Radius.circular(12),
          ),
          borderSide: const Border(
            left: BorderSide(color: AppColor.grey200, width: 1),
            top: BorderSide(color: AppColor.grey200, width: 1),
            bottom: BorderSide(color: AppColor.grey200, width: 1),
          ),
          index: 0,
          tabController: _tabController,
        ),
        TabWidget(
          text: AppString.arbeitgeber,
          borderRadius: BorderRadius.zero,
          borderSide: Border.all(color: AppColor.grey200, width: 1),
          index: 1,
          tabController: _tabController,
        ),
        TabWidget(
          text: AppString.temporaerbuero,
          borderRadius: const BorderRadius.only(
            topRight: Radius.circular(12),
            bottomRight: Radius.circular(12),
          ),
          borderSide: const Border(
            right: BorderSide(color: AppColor.grey200, width: 1),
            top: BorderSide(color: AppColor.grey200, width: 1),
            bottom: BorderSide(color: AppColor.grey200, width: 1),
          ),
          index: 2,
          tabController: _tabController,
        ),
      ],
      labelPadding: EdgeInsets.zero,
      isScrollable: true,
      indicator: ShapeDecoration(
        shape: RoundedRectangleBorder(
          borderRadius: _indicatorBorderRadius ??
              const BorderRadius.only(
                topLeft: Radius.circular(12),
                bottomLeft: Radius.circular(12),
              ),
        ),
        color: AppColor.cyanLight,
      ),
      indicatorSize: TabBarIndicatorSize.tab,
      labelColor: AppColor.white,
      unselectedLabelColor: AppColor.cyanDark,
      dividerColor: Colors.transparent,
      indicatorWeight: 0,
    );
  }

  Widget _buildTabBarView() {
    return ResponsiveBuilder(builder: (context, sizingInformation) {
      return SizedBox(
        // height: sizingInformation.isMobile
        //     ? 1200
        //     : sizingInformation.isTablet
        //         ? 1400
        //         : 1900,
        child: ContentSizeTabBarView(
          physics: const NeverScrollableScrollPhysics(),
          controller: _tabController,
          children: const [
            Center(child: FirstTab()),
            Center(child: SecondTab()),
            Center(child: ThirdTab()),
          ],
        ),
      );
    });
  }

  Widget _bottomNavigationWidget() {
    return Container(
      height: 90,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.only(
          topRight: Radius.circular(12),
          topLeft: Radius.circular(12),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.3),
            spreadRadius: 1,
            blurRadius: 2,
            offset: const Offset(0, 0.5),
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 20),
              padding: const EdgeInsets.symmetric(vertical: 4),
              width: double.infinity,
              child: CustomButton(
                buttonFontSize: 14,
                buttonHeight: 40,
                text: AppString.kostenlosRegistrieren,
                onPressed: () {},
                gradientColors: const [
                  AppColor.cyanDark,
                  AppColor.blueGradient
                ],
                borderRadius: 12.0,
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _tabController.removeListener(_handleTabSelection);
    _tabController.dispose();
    _scrollController.dispose();
    super.dispose();
  }
}
