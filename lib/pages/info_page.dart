import 'package:client_test_project/pages/home_page.dart';
import 'package:client_test_project/utils/app_color.dart';
import 'package:client_test_project/utils/app_string.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class InfoPage extends StatelessWidget {
  const InfoPage({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 10,
                        ),
                        RichText(
                          text: TextSpan(
                            style: Theme.of(context).textTheme.bodyLarge,
                            children: [
                              TextSpan(
                                text: AppString.yehYou,
                                style: GoogleFonts.lato(
                                  fontSize: 20,
                                ),
                              ),
                              WidgetSpan(
                                child: Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 2.0),
                                  child: Icon(Icons.waving_hand_sharp),
                                ),
                              ),
                              TextSpan(
                                text: AppString.nowYou,
                                style: GoogleFonts.lato(
                                  fontSize: 20,
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 25,
                        ),
                        Text(
                          AppString.nowYouCan,
                          style: GoogleFonts.lato(
                            fontSize: 20,
                          ),
                        ),
                        Text(
                          AppString.onlyFrontend,
                          style: GoogleFonts.lato(
                            fontSize: 18,
                          ),
                        ),
                        Text(
                          AppString.withFlutter,
                          style: GoogleFonts.lato(
                            fontSize: 18,
                          ),
                        ),
                        Text(
                          AppString.fullResponsive,
                          style: GoogleFonts.lato(
                            fontSize: 18,
                          ),
                        ),
                        const SizedBox(
                          height: 25,
                        ),
                        Text(
                          AppString.createTestPage,
                          style: GoogleFonts.lato(
                            fontSize: 20,
                          ),
                        ),
                        const SizedBox(
                          height: 25,
                        ),
                        Text(
                          AppString.sendMe,
                          style: GoogleFonts.lato(
                            fontSize: 20,
                          ),
                        ),
                        const SizedBox(
                          height: 25,
                        ),
                        Text(
                          AppString.websiteScroll,
                          style: GoogleFonts.lato(
                            fontSize: 20,
                          ),
                        ),
                      ],
                    )),
                const SizedBox(
                  height: 25,
                ),
                MaterialButton(
                    minWidth: double.infinity,
                    elevation: 0.0,
                    highlightColor: Colors.greenAccent,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        side: const BorderSide(width: 1, color: Colors.grey)),
                    color: AppColor.white,
                    onPressed: () => Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const HomePage())),
                    height: 50,
                    child: Text(
                      AppString.mobileView,
                      style: GoogleFonts.lato(
                          color: AppColor.cyanDark, fontSize: 16),
                    )),
                const SizedBox(
                  height: 15,
                ),
                MaterialButton(
                    elevation: 0.0,
                    minWidth: double.infinity,
                    highlightColor: Colors.greenAccent,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        side: const BorderSide(width: 1, color: Colors.grey)),
                    color: Colors.white,
                    onPressed: () {},
                    height: 50,
                    child: Text(
                      AppString.desktopView,
                      style: GoogleFonts.lato(
                          color: AppColor.cyanDark, fontSize: 16),
                    )),
                const SizedBox(
                  height: 15,
                ),
                MaterialButton(
                    elevation: 0.0,
                    minWidth: double.infinity,
                    highlightColor: Colors.greenAccent,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        side: BorderSide(width: 1, color: Colors.grey)),
                    color: Colors.white,
                    onPressed: () {},
                    height: 50,
                    child: Text(
                      AppString.desktopScroll,
                      style: GoogleFonts.lato(
                          color: AppColor.cyanDark, fontSize: 16),
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
