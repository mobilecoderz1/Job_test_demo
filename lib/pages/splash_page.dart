import 'dart:async';

import 'package:client_test_project/utils/app_color.dart';
import 'package:client_test_project/utils/app_string.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'info_page.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({super.key});

  @override
  State<SplashPage> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    Timer(const Duration(seconds: 3), () {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => const InfoPage()),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.darkBlue,
      body: ScreenTypeLayout.builder(
        mobile: (BuildContext context) =>
            const _SplashContent(padding: 30.0, width: double.infinity),
        tablet: (BuildContext context) =>
            const Center(child: _SplashContent(padding: 30.0, width: 450.0)),
        desktop: (BuildContext context) =>
            const Center(child: _SplashContent(padding: 30.0, width: 850.0)),
      ),
    );
  }
}

class _SplashContent extends StatelessWidget {
  final double padding;
  final double width;

  const _SplashContent({required this.padding, required this.width});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: Padding(
        padding: EdgeInsets.all(padding),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              height: 250,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [Image.asset('assets/images/logo.png')],
              ),
            ),
            const SizedBox(height: 50),
            Text(
              AppString.flutterTestIndex,
              style: GoogleFonts.lato(
                fontSize: 75,
                fontWeight: FontWeight.normal,
                color: AppColor.cyanDark,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
