import 'package:client_test_project/utils/app_color.dart';
import 'package:client_test_project/utils/app_string.dart';
import 'package:client_test_project/utils/desktop_layout_config.dart';
import 'package:client_test_project/utils/size_config.dart';
import 'package:client_test_project/widgets/step_widget1.dart';
import 'package:client_test_project/widgets/step_widget2.dart';
import 'package:client_test_project/widgets/step_widget3.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_builder/responsive_builder.dart';

class FirstTab extends StatelessWidget {
  const FirstTab({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        if (sizingInformation.deviceScreenType == DeviceScreenType.desktop) {
          return _buildDesktopLayout(SizeConfig.size);
        } else {
          return _buildMobileLayout();
        }
      },
    );
  }

  Widget _buildDesktopLayout(Size size) {
    var layoutConfig = getDesktopLayoutConfig(size);
    return ListView(
      padding: EdgeInsets.zero,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      children: [
        Center(
          child: Text(
            AppString.dreiEinfacheSchritte,
            style: GoogleFonts.lato(
              fontSize: 40,
              color: AppColor.black,
            ),
          ),
        ),
        Stack(
          children: [
            StepWidget1(
              bgCircleDesign: Container(
                height: size.height * 0.25,
                width: size.height * 0.25,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: AppColor.grey100,
                ),
              ),
              stepNumber: AppString.count1,
              description: AppString.erstellenLebenslauf,
              imagePath: 'assets/images/tab1_step1.png',
            ),
            Padding(
              padding: EdgeInsets.only(top: size.height * 0.54),
              child: const StepWidget2(
                stepNumber: AppString.count2,
                description: AppString.erstellenLebenslauf,
                backgroundPath: 'assets/images/step_2_bg.png',
                foregroundPath: 'assets/images/tab1_step2.png',
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: size.height),
              child: StepWidget3(
                bgCircleDesign: Container(
                  height: size.height * 0.35,
                  width: size.height * 0.35,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: AppColor.grey100,
                  ),
                ),
                stepNumber: AppString.count3,
                description: AppString.mitKlickBewerben,
                imagePath: 'assets/images/tab1_step3.png',
              ),
            ),
            Positioned(
              top: layoutConfig.line1TopMargin,
              left: layoutConfig.line1LeftMargin,
              child: Image.asset(
                "assets/images/line1.png",
                height: layoutConfig.line1Height,
                width: layoutConfig.line1Width,
              ),
            ),
            Positioned(
              top: layoutConfig.line2TopMargin,
              left: layoutConfig.line2LeftMargin,
              child: Image.asset(
                "assets/images/line2.png",
                height: layoutConfig.line2Height,
                width: layoutConfig.line2Width,
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 100,
        )
      ],
    );
  }

  Widget _buildMobileLayout() {
    return Column(
      children: [
        Center(
          child: Text(
            AppString.dreiEinfacheSchritte,
            style: GoogleFonts.lato(
              fontSize: 21,
              fontWeight: FontWeight.w500,
              color: AppColor.black,
            ),
          ),
        ),
        StepWidget1(
          bgCircleDesign: Container(),
          stepNumber: AppString.count1,
          description: AppString.erstellenLebenslauf,
          imagePath: 'assets/images/tab1_step1.png',
        ),
        const StepWidget2(
          stepNumber: AppString.count2,
          description: AppString.erstellenLebenslauf,
          backgroundPath: 'assets/images/step_2_bg_mobile.png',
          foregroundPath: 'assets/images/tab1_step2.png',
        ),
        StepWidget3(
          bgCircleDesign: Container(),
          stepNumber: AppString.count3,
          description: AppString.mitKlickBewerben,
          imagePath: 'assets/images/tab1_step3.png',
        ),
      ],
    );
  }
}
