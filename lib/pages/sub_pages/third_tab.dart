import 'package:client_test_project/utils/app_color.dart';
import 'package:client_test_project/utils/app_string.dart';
import 'package:client_test_project/utils/desktop_layout_config.dart';
import 'package:client_test_project/widgets/step_widget1.dart';
import 'package:client_test_project/widgets/step_widget2.dart';
import 'package:client_test_project/widgets/step_widget3.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_builder/responsive_builder.dart';

class ThirdTab extends StatelessWidget {
  const ThirdTab({super.key});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        if (sizingInformation.deviceScreenType == DeviceScreenType.desktop) {
          return _buildDesktopLayout(size);
        } else {
          return _buildMobileLayout();
        }
      },
    );
  }

  Widget _buildDesktopLayout(Size size) {
    var layoutConfig = getDesktopLayoutConfig(size);

    return ListView(
      padding: EdgeInsets.zero,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      children: [
        Center(
          child: Text(
            AppString.dreiEinfacheSchritteZurVermittlung,
            textAlign: TextAlign.center,
            style: GoogleFonts.lato(
              fontSize: 40,
              color: AppColor.black,
            ),
          ),
        ),
        Stack(
          children: [
            StepWidget1(
              bgCircleDesign: Container(
                height: size.height * 0.25,
                width: size.height * 0.25,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: AppColor.grey100,
                ),
              ),
              stepNumber: AppString.count1,
              description: AppString.erstellenUnternehmensprofil,
              imagePath: 'assets/images/tab1_step1.png',
            ),
            Padding(
              padding: EdgeInsets.only(top: size.height * 0.54),
              child: const StepWidget2(
                stepNumber: AppString.count2,
                description: AppString.vermittlungsangebot,
                backgroundPath: 'assets/images/step_2_bg.png',
                foregroundPath: 'assets/images/tab3_step2.png',
              ),
            ),
            const SizedBox(height: 20),
            Padding(
              padding: EdgeInsets.only(top: size.height),
              child: StepWidget3(
                bgCircleDesign: Container(
                  height: size.height * 0.35,
                  width: size.height * 0.35,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: AppColor.grey100,
                  ),
                ),
                stepNumber: AppString.count3,
                description: AppString.vermittlung,
                imagePath: 'assets/images/tab3_step3.png',
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                top: layoutConfig.line1TopMargin,
                left: layoutConfig.line1LeftMargin,
              ),
              child: Image.asset(
                "assets/images/line1.png",
                height: layoutConfig.line1Height,
                width: layoutConfig.line1Width,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                top: layoutConfig.line2TopMargin,
                left: layoutConfig.line2LeftMargin,
              ),
              child: Image.asset(
                "assets/images/line2.png",
                height: layoutConfig.line2Height,
                width: layoutConfig.line2Width,
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 100,
        )
      ],
    );
  }

  Widget _buildMobileLayout() {
    return Column(
      children: [
        Center(
          child: Text(
            AppString.dreiEinfacheSchritteZurVermittlung,
            textAlign: TextAlign.center,
            style: GoogleFonts.lato(
              fontSize: 21,
              fontWeight: FontWeight.w500,
              color: AppColor.black,
            ),
          ),
        ),
        StepWidget1(
          bgCircleDesign: Container(),
          stepNumber: AppString.count1,
          description: AppString.erstellenUnternehmensprofil,
          imagePath: 'assets/images/tab1_step1.png',
        ),
        const StepWidget2(
          stepNumber: AppString.count2,
          description: AppString.vermittlungsangebot,
          backgroundPath: 'assets/images/step_2_bg_mobile.png',
          foregroundPath: 'assets/images/tab3_step2.png',
        ),
        StepWidget3(
          bgCircleDesign: Container(),
          stepNumber: AppString.count3,
          description: AppString.vermittlung,
          imagePath: 'assets/images/tab3_step3.png',
        ),
      ],
    );
  }
}
