import 'package:flutter/material.dart';

class AppColor {
  /// Here We Define My All Colours
  static const Color grey = Color(0xff718096);
  static const Color grey100 = Color.fromRGBO(187, 187, 189, 0.1);
  static const Color grey200 = Color(0xffCBD5E0);
  static const Color cyanDark = Color(0xff319795);
  static const Color cyanLight = Color(0xff81E6D9);
  static const Color blueGradient = Color(0xff3182CE);
  static const Color cyanLight100 = Color(0xffE6FFFA);
  static const Color white = Color(0xffffffff);
  static const Color black = Color(0xff4A5568);
  static const Color darkBlue = Color(0xff1E1C44);
  static const Color cyanList200 = Color(0xff38B2AC);
}
