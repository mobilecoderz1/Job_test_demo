class AppString {
  /// General Strings
  static const applicationName = 'Client Test Project';

  /// Test Strings
  static const flutterTestIndex = 'Flutter\nTest\nIndex';
  static const yehYou = 'yeh you rocks';
  static const nowYou = ', now you are in next level ';
  static const nowYouCan =
      'Now you can create the Test page\nand show skilled you are';
  static const onlyFrontend = '-Only Frontend';
  static const withFlutter = '-With Flutter';
  static const fullResponsive = '-Full Responsive Webview';
  static const createTestPage = 'Create this Test page and push to the GitLab';
  static const sendMe =
      'Send me via Slack a message when you start the and when you will be finished';
  static const websiteScroll =
      'The website can be scrolled, the buttons, "Kostenlos" goes on the top bar';

  /// View Modes
  static const mobileView = 'Mobile View';
  static const desktopView = 'Desktop View';
  static const desktopScroll = 'Desktop Scroll';

  /// Registration
  static const kostenlosRegistrieren = 'Kostenlos Registrieren';
  static const login = 'Login';

  /// Job Website
  static const deineJobWebsite = 'Deine Job\nwebsite';
  static const deineJob = 'Deine Job';
  static const website = 'website';

  /// User Roles
  static const arbeitnehmer = 'Arbeitnehmer';
  static const arbeitgeber = 'Arbeitgeber';
  static const temporaerbuero = 'Temporärbüro';

  /// Steps
  static const dreiEinfacheSchritte =
      'Drei einfache Schritte\nzu deinem neuen Job';
  static const erstellenLebenslauf = 'Erstellen dein Lebenslauf';
  static const erstellenUnternehmensprofil =
      'Erstellen dein Unternehmensprofil';
  static const erstellenJobinserat = 'Erstellen ein Jobinserat';
  static const vermittlungsangebot =
      'Erhalte Vermittlungs-\nangebot von Arbeitgeber';

  /// Application Steps
  static const mitKlickBewerben = 'Mit nur einem Klick\nbewerben';
  static const vermittlung = 'Vermittlung nach\nProvision oder\nStundenlohn';

  static const dreiEinfache =
      'Drei einfache Schritte\nzu deinem neuen Mitarbeiter';
  static const wahleDeinenNeuen = 'Wähle deinen\nneuen Mitarbeiter aus';
  static const dreiEinfacheSchritteZurVermittlung =
      'Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter';

  /// Counts
  static const count1 = '1.';
  static const count2 = '2.';
  static const count3 = '3.';
}
