import 'package:flutter/material.dart';

class DesktopLayoutConfig {
  final double line1Height;
  final double line1Width;
  final double line2Height;
  final double line2Width;
  final double line1TopMargin;
  final double line1LeftMargin;
  final double line2TopMargin;
  final double line2LeftMargin;

  DesktopLayoutConfig({
    required this.line1Height,
    required this.line1Width,
    required this.line2Height,
    required this.line2Width,
    required this.line1TopMargin,
    required this.line1LeftMargin,
    required this.line2TopMargin,
    required this.line2LeftMargin,
  });
}

DesktopLayoutConfig getDesktopLayoutConfig(Size size) {
  if (size.width > 950 && size.width < 1000) {
    return DesktopLayoutConfig(
      line1Height: size.height * 0.34,
      line1Width: size.width * 0.59,
      line2Height: size.height * 0.28,
      line2Width: size.width * 0.55,
      line1TopMargin: size.height * 0.37,
      line1LeftMargin: 0.0,
      line2TopMargin: size.height * 0.9,
      line2LeftMargin: size.width * 0.06,
    );
  } else if (size.width > 1001 && size.width < 1200) {
    return DesktopLayoutConfig(
      line1Height: size.height * 0.34,
      line1Width: size.width * 0.67,
      line2Height: size.height * 0.28,
      line2Width: size.width * 0.55,
      line1TopMargin: size.height * 0.36,
      line1LeftMargin: 0.0,
      line2TopMargin: size.height * 0.9,
      line2LeftMargin: size.width * 0.06,
    );
  } else if (size.width > 1201 && size.width < 1350) {
    return DesktopLayoutConfig(
      line1Height: size.height * 0.33,
      line1Width: size.width * 0.7,
      line2Height: size.height * 0.28,
      line2Width: size.width * 0.55,
      line1TopMargin: size.height * 0.35,
      line1LeftMargin: 0.0,
      line2TopMargin: size.height * 0.9,
      line2LeftMargin: size.width * 0.06,
    );
  } else if (size.width > 1351 && size.width < 1450) {
    return DesktopLayoutConfig(
      line1Height: size.height * 0.32,
      line1Width: size.width * 0.62,
      line2Height: size.height * 0.28,
      line2Width: size.width * 0.55,
      line1TopMargin: size.height * 0.36,
      line1LeftMargin: size.width * 0.06,
      line2TopMargin: size.height * 0.88,
      line2LeftMargin: size.width * 0.13,
    );
  } else {
    return DesktopLayoutConfig(
      line1Height: size.height * 0.34,
      line1Width: size.width * 0.59,
      line2Height: size.height * 0.28,
      line2Width: size.width * 0.55,
      line1TopMargin: size.height * 0.3,
      line1LeftMargin: size.width * 0.15,
      line2TopMargin: size.height * 0.8,
      line2LeftMargin: size.width * 0.18,
    );
  }
}
