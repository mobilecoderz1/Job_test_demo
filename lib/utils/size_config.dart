import 'package:flutter/widgets.dart';

class SizeConfig {
  static MediaQueryData? _mediaQueryData;
  static Size size = const Size(0, 0);
  static double screenWidth = 0;
  static double screenHeight = 0;

  static void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    size = _mediaQueryData!.size;
    screenWidth = _mediaQueryData!.size.width;
    screenHeight = _mediaQueryData!.size.height;

  }
}
