import 'package:client_test_project/utils/app_string.dart';
import 'package:client_test_project/utils/size_config.dart';
import 'package:client_test_project/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_builder/responsive_builder.dart';

import '../utils/app_color.dart';

class BannerWidget extends StatelessWidget {
  const BannerWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        SizeConfig.init(context);
        return sizingInformation.isMobile
            ? _buildMobileBanner(SizeConfig.size)
            : _buildNonMobileBanner(sizingInformation, SizeConfig.size);
      },
    );
  }

  Widget _buildMobileBanner(Size size) {
    return SingleChildScrollView(
      child: Stack(
        children: [
          Container(
            height: 650,
            width: double.infinity,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/banner_bg_mobile.png"),
                fit: BoxFit.fill,
                alignment: Alignment.topCenter,
              ),
            ),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Text(
                    AppString.deineJobWebsite,
                    textAlign: TextAlign.center,
                    style: GoogleFonts.lato(
                        color: AppColor.black,
                        fontSize: 42,
                        fontWeight: FontWeight.w500),
                  ),
                ),
                Image.asset(
                  height: 405,
                  "assets/images/handshake.png",
                  width: size.width,
                  fit: BoxFit.contain,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildNonMobileBanner(SizingInformation sizingInformation, Size size) {
    var titleFontSize =
        sizingInformation.deviceScreenType == DeviceScreenType.tablet
            ? 65.0
            : 65.0;
    var buttonWidth =
        sizingInformation.deviceScreenType == DeviceScreenType.tablet
            ? 200.0
            : 300.0;
    var buttonFontSize =
        sizingInformation.deviceScreenType == DeviceScreenType.tablet
            ? 12.0
            : 14.0;
    var buttonHeight =
        sizingInformation.deviceScreenType == DeviceScreenType.tablet
            ? 30.0
            : 40.0;

    return Container(
      height: size.height * 0.66,
      width: size.width,
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/banner_bg.png"),
          fit: BoxFit.fill,
          alignment: Alignment.topCenter,
        ),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: size.width * 0.09),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  AppString.deineJob,
                  textAlign: TextAlign.start,
                  style: GoogleFonts.lato(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: titleFontSize,
                      ),
                      height: 1),
                ),
                SizedBox(height: size.height * 0.01),
                Text(
                  AppString.website,
                  textAlign: TextAlign.start,
                  style: GoogleFonts.lato(
                      fontSize: titleFontSize,
                      height: 1,
                      fontWeight: FontWeight.w700),
                ),
                SizedBox(height: size.height * 0.04),
                Container(
                  width: buttonWidth,
                  padding: const EdgeInsets.symmetric(vertical: 2),
                  child: CustomButton(
                    buttonHeight: buttonHeight,
                    buttonFontSize: buttonFontSize,
                    text: AppString.kostenlosRegistrieren,
                    onPressed: () {},
                    gradientColors: const [
                      AppColor.cyanDark,
                      AppColor.blueGradient,
                    ],
                    borderRadius: 12.0,
                  ),
                ),
              ],
            ),
            // sizingInformation.isTablet
            //     ? const SizedBox(
            //         width: 143,
            //       )
            //     : const SizedBox(
            //         width: 10,
            //       ),
            Container(
              height: size.height * 0.5,
              width: size.width * 0.4,
              decoration: const BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage("assets/images/handshake.png"),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
