import 'package:client_test_project/utils/app_color.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;
  final List<Color> gradientColors;
  final double borderRadius;
  final double buttonHeight;
  final double buttonFontSize;

  const CustomButton({
    super.key,
    required this.text,
    required this.onPressed,
    required this.gradientColors,
    this.borderRadius = 12.0,
    this.buttonHeight = 40.0,
    this.buttonFontSize = 14.0,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: buttonHeight,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: gradientColors,
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
        borderRadius: BorderRadius.circular(borderRadius),
      ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.transparent,
          foregroundColor: AppColor.white,
          shadowColor: Colors.transparent,
          overlayColor: AppColor.black,
          textStyle: GoogleFonts.lato(
            fontSize: buttonFontSize,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadius),
          ),
        ),
        onPressed: onPressed,
        child: Text(
          text,
          style: GoogleFonts.lato(),
        ),
      ),
    );
  }
}
