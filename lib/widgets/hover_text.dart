import 'package:flutter/material.dart';

class HoverText extends StatefulWidget {
  final String text;
  final TextStyle normalStyle;
  final TextStyle hoverStyle;
  final Color hoverColor;

  const HoverText({
    required this.text,
    required this.normalStyle,
    required this.hoverStyle,
    required this.hoverColor,
    Key? key,
  }) : super(key: key);

  @override
  _HoverTextState createState() => _HoverTextState();
}

class _HoverTextState extends State<HoverText> {
  bool _isHovered = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (_) {
        setState(() {
          _isHovered = true;
        });
      },
      onExit: (_) {
        setState(() {
          _isHovered = false;
        });
      },
      child: Stack(
        alignment: Alignment.centerLeft,
        children: [
          Text(
            widget.text,
            style: _isHovered
                ? widget.hoverStyle.copyWith(color: widget.hoverColor)
                : widget.normalStyle,
          ),
          if (_isHovered)
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                height: 1,
                color: widget.hoverColor,
              ),
            ),
        ],
      ),
    );
  }
}
