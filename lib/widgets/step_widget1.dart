import 'package:client_test_project/utils/app_color.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_builder/responsive_builder.dart';

class StepWidget1 extends StatelessWidget {
  final String stepNumber;
  final String description;
  final String imagePath;
  final Widget bgCircleDesign;

  const StepWidget1({
    required this.stepNumber,
    required this.description,
    required this.imagePath,
    required this.bgCircleDesign,
  });

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
        mobile: (BuildContext context) => Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 30.0, horizontal: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 90),
                    child: Text(
                      stepNumber,
                      style: GoogleFonts.lato(
                        fontSize: 130,
                        color: AppColor.grey,
                      ),
                    ),
                  ),
                  Column(
                    children: [
                      Image.asset(
                        imagePath,
                        height: 145,
                        width: 220,
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 10.0),
                        child: Text(
                          description,
                          style: GoogleFonts.lato(
                            fontSize: 16,
                            color: AppColor.grey,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
        tablet: (BuildContext context) => Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 30.0, horizontal: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    stepNumber,
                    style: GoogleFonts.lato(
                      fontSize: 130,
                      color: AppColor.grey,
                    ),
                  ),
                  Column(
                    children: [
                      Image.asset(
                        imagePath,
                        height: 180,
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 10.0),
                        child: Text(
                          description,
                          style: GoogleFonts.lato(
                            fontSize: 16,
                            color: AppColor.grey,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
        desktop: (BuildContext context) => Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 50.0, horizontal: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Stack(
                    children: [
                      bgCircleDesign,
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: Text(
                          stepNumber,
                          style: GoogleFonts.lato(
                            fontSize: 130,
                            color: AppColor.grey,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(width: 20),
                  Container(
                    margin: const EdgeInsets.only(top: 70.0),
                    child: Text(
                      description,
                      style: GoogleFonts.lato(
                        fontSize: 30,
                        color: AppColor.grey,
                      ),
                    ),
                  ),
                  const SizedBox(width: 20),
                  Image.asset(
                    imagePath,
                    height: 253,
                    width: 384,
                  ),
                ],
              ),
            ));
  }
}
