import 'package:client_test_project/utils/app_color.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_builder/responsive_builder.dart';

class StepWidget2 extends StatelessWidget {
  final String stepNumber;
  final String description;
  final String backgroundPath;
  final String foregroundPath;

  const StepWidget2({
    required this.stepNumber,
    required this.description,
    required this.backgroundPath,
    required this.foregroundPath,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
          child: Image.asset(height: 250, backgroundPath, fit: BoxFit.fill),
        ),
        ScreenTypeLayout.builder(
          mobile: (BuildContext context) => Padding(
            padding: const EdgeInsets.only(top: 30.0, bottom: 60.0, left: 30.0),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      stepNumber,
                      style: GoogleFonts.lato(
                        fontSize: 130,
                        color: AppColor.grey,
                      ),
                    ),
                    const SizedBox(width: 20),
                    Container(
                      margin: const EdgeInsets.only(top: 70.0),
                      child: Text(
                        description,
                        style: GoogleFonts.lato(
                          fontSize: 16,
                          color: AppColor.grey,
                        ),
                      ),
                    ),
                  ],
                ),
                Image.asset(
                  foregroundPath,
                  width: 181,
                  height: 127,
                ),
                const SizedBox(width: 20),
              ],
            ),
          ),
          tablet: (BuildContext context) => Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 50.0, horizontal: 10.0),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      stepNumber,
                      style: GoogleFonts.lato(
                        fontSize: 100,
                        color: AppColor.grey,
                      ),
                    ),
                    const SizedBox(width: 20),
                    Container(
                      margin: const EdgeInsets.only(top: 70.0),
                      child: Text(
                        description,
                        style: GoogleFonts.lato(
                          fontSize: 30,
                          color: AppColor.grey,
                        ),
                      ),
                    ),
                  ],
                ),
                Image.asset(
                  foregroundPath,
                  height: 180,
                ),
                const SizedBox(width: 20),
              ],
            ),
          ),
          desktop: (BuildContext context) => Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 50.0, horizontal: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  foregroundPath,
                  width: 324,
                  height: 227,
                ),
                const SizedBox(width: 70),
                Text(
                  stepNumber,
                  style: GoogleFonts.lato(
                    fontSize: 130,
                    color: AppColor.grey,
                  ),
                ),
                const SizedBox(width: 20),
                Container(
                  margin: const EdgeInsets.only(top: 70.0),
                  child: Text(
                    description,
                    style: GoogleFonts.lato(
                      fontSize: 30,
                      color: AppColor.grey,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
