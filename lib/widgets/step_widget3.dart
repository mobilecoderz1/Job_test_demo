import 'package:client_test_project/utils/app_color.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_builder/responsive_builder.dart';

class StepWidget3 extends StatelessWidget {
  final String stepNumber;
  final String description;
  final String imagePath;
  final Widget bgCircleDesign;

  const StepWidget3({
    required this.stepNumber,
    required this.description,
    required this.imagePath,
    required this.bgCircleDesign,
  });

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
        mobile: (BuildContext context) => Padding(
              padding: const EdgeInsets.only(
                left: 40.0,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        stepNumber,
                        style: GoogleFonts.lato(
                          fontSize: 130,
                          color: AppColor.grey,
                        ),
                      ),
                      const SizedBox(width: 20),
                      Container(
                        margin: const EdgeInsets.only(top: 10.0),
                        child: Text(
                          description,
                          style: GoogleFonts.lato(
                            fontSize: 16,
                            color: AppColor.grey,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Transform.translate(
                    offset: const Offset(0, -25),
                    child: Image.asset(
                      imagePath,
                      width: 281,
                      height: 210,
                    ),
                  ),
                ],
              ),
            ),
        tablet: (BuildContext context) => Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 30.0, horizontal: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    stepNumber,
                    style: GoogleFonts.lato(
                      fontSize: 130,
                      color: AppColor.grey,
                    ),
                  ),
                  Column(
                    children: [
                      Image.asset(
                        imagePath,
                        height: 180,
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 10.0),
                        child: Text(
                          description,
                          style: GoogleFonts.lato(
                            fontSize: 16,
                            color: AppColor.grey,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
        desktop: (BuildContext context) => Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 50.0, horizontal: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Stack(
                    children: [
                      bgCircleDesign,
                      Transform.translate(
                        offset: const Offset(0, -20),
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                left: 80.0,
                              ),
                              child: Text(
                                stepNumber,
                                style: GoogleFonts.lato(
                                  fontSize: 130,
                                  color: AppColor.grey,
                                ),
                              ),
                            ),
                            const SizedBox(width: 10),
                            Container(
                              margin: const EdgeInsets.only(top: 10.0),
                              child: Text(
                                description,
                                style: GoogleFonts.lato(
                                  fontSize: 30,
                                  color: AppColor.grey,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(width: 20),
                  Image.asset(
                    imagePath,
                    height: 376,
                    width: 502,
                  ),
                ],
              ),
            ));
  }
}
