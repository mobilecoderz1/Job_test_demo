import 'package:client_test_project/utils/app_color.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TabWidget extends StatefulWidget {
  final String text;
  final BorderRadius borderRadius;
  final Border borderSide;
  final int index;
  final TabController tabController;

  const TabWidget({
    required this.text,
    required this.borderRadius,
    required this.borderSide,
    required this.index,
    required this.tabController,
    Key? key,
  }) : super(key: key);

  @override
  _TabWidgetState createState() => _TabWidgetState();
}

class _TabWidgetState extends State<TabWidget> {
  bool _isHovered = false;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        widget.tabController.animateTo(widget.index);
      },
      onHover: (hovering) {
        setState(() {
          _isHovered = hovering;
        });
      },
      splashColor: Colors.transparent,
      hoverColor: AppColor.cyanLight100, // Define your hover color here
      borderRadius: widget.borderRadius,
      child: Tab(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: widget.borderRadius,
            border: widget.borderSide,
            color: _isHovered
                ? AppColor.cyanLight100.withOpacity(0.2)
                : Colors.transparent,
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40),
            child: Align(
              alignment: Alignment.center,
              child: Text(
                widget.text,
                style:
                    GoogleFonts.lato(fontWeight: FontWeight.w600, fontSize: 14),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
